# -*- coding: utf-8 -*-

def untyped_function(value1, value2):
    """Task10
    Add typing annotations. Think about which types are possible and be as unrestrictive as possible without using 'any'
    Complete this docstring
    """

    x = 0
    for i in value1:
        x += i
    if x < 0:
        print(value2)
    return x


def strings_to_numbers(input: tuple[str]) -> list:
    """Task11
    Each item from input is checked if it can be converted to a built-in number type and added to a list. If not then None should be added to the return list
    All converted values will be returned in a list. 

    Args:
        input (tuple[str])
    
    Returns:
        list: all converted numbers, or None for non numeric string items
    """

    ...


def define_types_for_this_function(x, y):
    """Task12
    Create the custom types Foo, Bar and Baz. 
    Foo: is a type of either a list or tuple which contain numbers of the built-in types
    Bar: is should describe a dict type which has Foo as keys and str as possible value types
    Baz: is either str or None
    The parameter x is of type Foo, the parameter y of type Bar and the function returns an object of type Baz
    The function checks each item from x if it is a key in y. If so, concatenate the values
    Add type annotations to the function

    Args:
        x (Foo): 
        y (Bar): 
    Returns: 
        Baz: concatenated string
    """

    ...
